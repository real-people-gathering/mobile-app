import { Component, Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable, from} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})

export class AuthService{

url = "http://api.thegame.fail";
userid = "dbcc7b8e-4132-4f7b-8537-2e8ffe44ae9c";
constructor(private http: HttpClient) {}

getUser(Userid): Observable<any>{
  return this.http.get(`${this.url}/user/${Userid}`, {})
  
}
}