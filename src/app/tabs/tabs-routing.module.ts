import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'auth',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../auth/auth.module').then(m => m.authPageModule)
          }
        ]
      },
      {
        path: 'timeline',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../Timeline/timeline.module').then(m => m.TimelinePageModule)
          },
          {
            path: 'post/:id',
            children:[{
              path: '',
              loadChildren: () =>
              import('../post/postmodal.module').then(m => m.PostModalPageModule)
            }
          ]
          }

        ]
      },
      {
        path: 'users',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../Users/users.module').then(m => m.UsersPageModule)
          },{
            path: 'following/:id',
            children:[{
              path: '',
              loadChildren: () =>
              import('../following/following.module').then(m => m.FollowingPageModule)
            }
        ]
      }
    ]
  }
      ,
      {
        path: '',
        redirectTo: '/tabs/auth',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/auth',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
